public class CD {
    private String titolo, autore;
    private int numeroBrani, durata;
    public CD(){
    }
    public CD(String titolo, String autore, int numeroBrani, int durata){
        this.titolo=titolo;
        this.autore=autore;
        this.numeroBrani=numeroBrani;
        this.durata=durata;
    }

    public String getTitolo() {
        return this.titolo;
    }

    public void setTitolo(String titolo) {
        this.titolo = titolo;
    }

    public String getAutore() {
        return this.autore;
    }

    public void setAutore(String autore) {
        this.autore = autore;
    }

    public int getNumeroBrani() {
        return this.numeroBrani;
    }

    public void setNumeroBrani(int numeroBrani) {
        this.numeroBrani = numeroBrani;
    }

    public int getDurata() {
        return this.durata;
    }

    public void setDurata(int durata) {
        this.durata = durata;
    }
    public String toString() {
        return "Titolo "+getTitolo()+
            ", Autore " + getAutore() +
            ", Numero Brani " + getNumeroBrani() +
            ", Durata " + getDurata();
    }

    public int comparaDurata(CD cd){
        int r=0;
        if(this.durata<cd.durata){
            r=-1;
        }
        if(this.durata>cd.durata){
            r=1;
        }
        if(this.durata==cd.durata){
            r=0;
        }
        return r;
    }
}