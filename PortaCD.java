public class PortaCD {
    private static int n;
    private static CD[] dischi;

    public PortaCD(int n) {
        PortaCD.n = n;
        dischi = new CD[n];
    }

    public CD getCD(int pos) {
        return dischi[pos];
    }

    public static void setCD(int pos, CD cd) {
        dischi[pos] = cd;
    }

    public static void killCD(int pos) {
        dischi[pos] = null;
    }

    public static int getNumMaxDischi() {
        return n;
    }

    public static int getN() {
        int count = 0;
        for (int i = 0; i < n; i++) {
            if (dischi[i] != null) {
                count++;
            }
        }
        return count;
    }

    public static CD cercaCDperTitolo(String titolo) {
        for (int i = 0; i < n; i++) {
            if (dischi[i] != null && dischi[i].getTitolo().equals(titolo)) {
                return dischi[i];
            }
        }
        return null;
    }

    @Override
    public String toString() {
        String s = "";
        for (int i = 0; i < n; i++) {
            if (dischi[i] != null) {
                s += dischi[i].toString() + "\n";
            }
        }
        return s;
    }

    public static int confrontaCollezione(PortaCD portaCD1, PortaCD portaCD2) {
        int numCdInComune = 0;

        for (int i = 0; i < PortaCD.getNumMaxDischi(); i++) {
            CD cd1 = portaCD1.getCD(i);
            for (int j = 0; j < PortaCD.getNumMaxDischi(); j++) {
                CD cd2 = portaCD2.getCD(j);
                if (cd1 != null && cd2 != null && cd1.equals(cd2)) {
                    numCdInComune++;
                }
            }
        }

        return numCdInComune;
    }
}