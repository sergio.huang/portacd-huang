public class App {
    public static void main(String[] args) throws Exception {
        CD primoCD=new CD("Panda", "Rosso", 5, 20);
        System.out.println(primoCD);
        System.out.println(primoCD.getTitolo());
        System.out.println(primoCD.getAutore());
        System.out.println(primoCD.getNumeroBrani());
        System.out.println(primoCD.getDurata());
        primoCD.setTitolo("Rosso");
        primoCD.setAutore("Rosso");
        primoCD.setNumeroBrani(3);
        primoCD.setDurata(15);
        System.out.println(primoCD);
        CD secondoCD=new CD("Orso", "Bianco", 5, 30);
        System.out.println(primoCD.comparaDurata(secondoCD));
        PortaCD portaCD1=new PortaCD(5);
        PortaCD portaCD2=new PortaCD(4);
        PortaCD.setCD(1, primoCD);
        PortaCD.setCD(2, primoCD);
        System.out.println(portaCD1);
        PortaCD.killCD(1);
        System.out.println(portaCD1);
        System.out.println(PortaCD.cercaCDperTitolo("Orso"));
        System.out.println(PortaCD.getN());
        System.out.println(PortaCD.getNumMaxDischi());
        System.out.println(PortaCD.confrontaCollezione(portaCD1, portaCD2));
        System.out.println(portaCD1);
        System.out.println(portaCD2);
        }
}
